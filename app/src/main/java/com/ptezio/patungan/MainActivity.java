package com.ptezio.patungan;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.menu_home)
    public LinearLayout menuHome;

    @BindView(R.id.menu_notif)
    public LinearLayout menuNotif;

    @BindView(R.id.menu_transaksi)
    public LinearLayout menuTransaksi;

    @BindView(R.id.menu_teman)
    public RelativeLayout menuTeman;

    @BindView(R.id.menu_cash_flow)
    public LinearLayout menuCashFlow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        setBottomNavigation(this);
    }

    public void setBottomNavigation(final Activity activity){

        ImageView icon = (ImageView) menuHome.findViewById(R.id.icon_home);
        icon.setColorFilter(getResources().getColor(R.color.blue));

        menuHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(activity, ServicesActivity.class));
                finish();
                overridePendingTransition(0, 0);
            }
        });

        menuNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(activity, ServicesActivity.class));
                finish();
                overridePendingTransition(0, 0);
            }
        });

        menuTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(activity, ServicesActivity.class));
                finish();
                overridePendingTransition(0, 0);
            }
        });

        menuTeman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(activity, BillingInfoActivity.class));
                finish();
                overridePendingTransition(0, 0);
            }
        });

        menuCashFlow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(activity, ProfileActivity.class));
                finish();
                overridePendingTransition(0, 0);
            }
        });

    }

}
